# Aplicación club de videos

## Ejecutar valores iniciales

```sh
mongo localhost:27017/videoclub init.js
```

Ajustar configuracion de conexione a la BD:

- Crear un archivo `config.py` en el directorio raíz en base al contenido de `config.py.sample` y ajustar la URI para la conexión con el replicaset de mongoDB.
pelic
## Modelo de datos

```javascript
peliculas:{
	duracion: 19,
	genero: 'comedia',
	titulo: 'thro',
	año: 2001,
	nominaciones: [
		{
			nombre_premio: "Oscar",
			detalle: "Mejor pelicula",
			año: 2002,
			ganado: false,
		},
		{
			nombre_premio: "Plullitzer",
			detalle: "Mejor comedia",
			año: 2001,
			ganado: false
		},
		{
			nombre_premio: "Las americas",
			detalle: "Comedia extranjera",
			año: 2002,
			ganado: true
		}
	],
	actores_principales: [
		{
			nombre: "Rafi Montes",
			interpretacion: "Wilson Martinez",
		},
		{
			nombre: "Eduarda Segoniz",
			interpretacion: "Luz Lanner Gosalvez"
		}
	],
	costo_unitario: 12,
	compañía: "SLNE Prodcciones"
};

clientes: {
	nombres: "Eugenio",
	ap_paterno: "Rojos",
	ap_materno: "Salchieq",
	ci: "8581812",
	no_targeta: "85818280x0123",
	betado: false,
};

prestamos_peliculas: {
	cliente: ObjectId('cliente'),
	copias_peliculas: [ 
		{ 
			copia_pelicula: ObjectId(copias_'peliculas'),
		},
		{
			copia_pelicula: ObjectId(copias_'peliculas'),
		}
	],
	fecha_prestamo: "2020/01/16 14:32:11",
	fecha_devolucion: "2020/02/02 14:32:11",
	dias_prestamo: 1,
	retornado: true,
	pago: {
		fecha_pago: "20/02/02 14:31:31",
		monto: 4
	}
};

copias_peliculas: {
	pelicula: Object_id('pelicua'),
	creacion: "2019/12/21 14:00:00",
	bajas: [
		{
			razon: "Robo",
			fecha: "2019/12/11 21:00:12" // AAAA/MM/DD hh:mm:ss
		},
	],
};

costos_y_descuentos: {
	costos: [
		{
			cantidad: 1,
			costo: 2
		},
		{
			cantidad: 2,
			costo: 3
		},
				{
			cantidad: 3,
			costo: 4
		},
				{
			cantidad: 4,
			costo: 5
		},
		{
			cantidad: 5,
			costo: 6
		},
	],
	descuentos: [
		{
			cantidad_minima: 3,
			cantidad_maxima: 5
			descuento: 0.05
		},
		{
			cantidad_minima: 6,
			cantidad_maxima: 0,
			descuento: 0.10
		}
	]
}
```
