from flask_pymongo import PyMongo
from pymongo import MongoClient

import click
from flask import current_app, g
from flask.cli import with_appcontext
from time import sleep
from config import DATABASE_URI

class DB(object):

    @staticmethod
    def init(app):
        # mongo -u admin -p toor 159.203.186.56:27018/admin
        #uri = 'mongodb://localhost:27017'
        uri = DATABASE_URI
        print('DATABASE_URI', uri)
        # uri = 'mongodb://admin:toor@159.203.186.56:27018/?authSource=admin&authMechanism=SCRAM-SHA-256'
        #client = MongoClient(uri)

        # uri = 'mongodb://admin:toor@159.203.186.56:27018,159.203.186.56:27017,159.203.186.56:27019/?authSource=admin&replicaSet=rs0'
        # uri = 'mongodb://admin:toor@mongo-rs0-2:27018,mongo-rs0-1:27017,mongo-rs0-3:27019/?authSource=admin&replicaSet=rs0'
        # mongodb://myDBReader:D1fficultP%40ssw0rd@mongodb0.example.com:27017,mongodb1.example.com:27017,mongodb2.example.com:27017/?authSource=admin&replicaSet=myRepl
        client = MongoClient(uri)
        print('Nodes:', client.nodes)
        print('Nodes:', client.nodes)
        try:
            DB.DATABASE = client.videoclub # BD videoclub
            DB.CLIENT = client
            print('db.peliculas.find().count():', str(DB.DATABASE['peliculas'].find({}).count()))
        except Exception as E:
            raise E

        # config = {'_id': 'rsvideoclub', 'members': [
        #     {'_id': 0, 'host': '159.203.186.56:27017'},
        #     {'_id': 1, 'host': '159.203.186.56:27018'},
        #     {'_id': 2, 'host': '159.203.186.56:27019'},
        # ]}
        # print('replSetInitiate:', cliente.admin.command("replSetInitiate", config))
        # configurando conexion a replicaset
        #client = MongoClient(['159.203.186.56:27017', '159.203.186.56:27018', '159.203.186.56:27019'], replicaset='prueba')

        # conectando al replicaset
        

        
        #DB.DATABASE = client['tvideos_app']

    @staticmethod
    def insert(collection, data):
        DB.DATABASE[collection].insert(data)

    @staticmethod
    def find_one(collection, query):
        return DB.DATABASE[collection].find_one(query)

    @staticmethod
    def find(collection, query={}):
        return DB.DATABASE[collection].find(query)

    @staticmethod
    def update_one(collection, query={}, sett={}):
        return DB.DATABASE[collection].update_one(query, sett)

    @staticmethod
    def status():
        # retorna datos de conexion
        ''' Ejemplo
        ---- Nodes ---
         frozenset({('mongo-rs0-2', 27017), ('mongo-rs0-3', 27017), ('mongo-rs0-1', 27017)})
         ---- Primary ---
         ('mongo-rs0-1', 27017)
         ---- Secondaries ---
         {('mongo-rs0-2', 27017), ('mongo-rs0-3', 27017)}
         --------- Server info ----------------
         {'version': '4.2.3', 'gitVersion': '6874650b362138df74be53d366bbefc321ea32d4', 'modules': [], 'allocator': 'tcmalloc', 'javascriptEngine': 'mozjs', 'sysInfo': 'deprecated', 'versionArray': [4, 2, 3, 0], 'openssl': {'running': 'OpenSSL 1.1.1  11 Sep 2018', 'compiled': 'OpenSSL 1.1.1  11 Sep 2018'}, 'buildEnvironment': {'distmod': 'ubuntu1804', 'distarch': 'x86_64', 'cc': '/opt/mongodbtoolchain/v3/bin/gcc: gcc (GCC) 8.2.0', 'ccflags': '-fno-omit-frame-pointer -fno-strict-aliasing -ggdb -pthread -Wall -Wsign-compare -Wno-unknown-pragmas -Winvalid-pch -Werror -O2 -Wno-unused-local-typedefs -Wno-unused-function -Wno-deprecated-declarations -Wno-unused-const-variable -Wno-unused-but-set-variable -Wno-missing-braces -fstack-protector-strong -fno-builtin-memcmp', 'cxx': '/opt/mongodbtoolchain/v3/bin/g++: g++ (GCC) 8.2.0', 'cxxflags': '-Woverloaded-virtual -Wno-maybe-uninitialized -fsized-deallocation -std=c++17', 'linkflags': '-pthread -Wl,-z,now -rdynamic -Wl,--fatal-warnings -fstack-protector-strong -fuse-ld=gold -Wl,--build-id -Wl,--hash-style=gnu -Wl,-z,noexecstack -Wl,--warn-execstack -Wl,-z,relro', 'target_arch': 'x86_64', 'target_os': 'linux'}, 'bits': 64, 'debug': False, 'maxBsonObjectSize': 16777216, 'storageEngines': ['biggie', 'devnull', 'ephemeralForTest', 'wiredTiger'], 'ok': 1.0, '$clusterTime': {'clusterTime': Timestamp(1585864465, 1), 'signature': {'hash': b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00', 'keyId': 0}}, 'operationTime': Timestamp(1585864465, 1)}
        
        Retorna un diccionario con { nodes, primary, secondaries, server_info}
        '''
        # print('---- Nodes ---')
        # print(DB.CLIENT.nodes)
        # print('---- Primary ---')
        # print(DB.CLIENT.primary)
        # print('---- Secondaries ---')
        # print(DB.CLIENT.secondaries)
        # print('--------- Server info ----------------')
        # print(DB.CLIENT.server_info())
        return {
            'nodes': DB.CLIENT.nodes,
            'primary': DB.CLIENT.primary,
            'secondaries': DB.CLIENT.secondaries,
            'server_info': DB.CLIENT.server_info()
        }
    
# def get_db(app):
#     if 'db' not in g:
#         app.config["MONGO_URI"] = 
#         g.db = PyMongo(app)
#     return g.db

# def iniciarBd(app):
#     # se pordiran ejecutar sentencias para dar valores iniciales ala BD
#     db = get_db(app)

    
# def cerrarBd(e=None):
#     db = g.pop('db', None)
#     if db is not None:
#         db.close()

# @click.command('init-db')
# @with_appcontext
# def init_db_command(app):
#     """Clear the existing data and create new tables."""
#     iniciarBd(app)
#     click.echo('Base de datos iniciada.')

# def init_app(app):
#     app.teardown_appcontext(cerrarBd)
#     app.cli.add_command(init_db_command)
