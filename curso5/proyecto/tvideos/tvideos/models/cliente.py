import datetime

from tvideos.database import DB
from bson.objectid import ObjectId

class Cliente(object):
    def __init__(self, **kwargs):

        self.nombres = kwargs['nombres']
        self.ap_paterno = kwargs['ap_paterno']
        self.ap_materno = kwargs['ap_materno']

        if kwargs.get('ci', None) is None:
            self.ci = ''
        else:
            self.ci = kwargs['ci']
        if kwargs.get('fecha_nacimiento', None) is None:
            self.fecha_nacimiento = ''
        else:
            try:
                datetime.datetime.strptime(kwargs['fecha_nacimiento'], '%d/%m/%Y')
                self.fecha_nacimiento = kwargs['fecha_nacimiento']
            except ValueError:
                raise ValueError("Incorrect data format, should be DD/MM/YYYY")
        if kwargs.get('email', None) is None:
            self.email = ''
        else:
            self.email = kwargs['email']
        if kwargs.get('direccion', None) is None:
            self.direccion = ''
        else:
            self.direccion = kwargs['direccion']
        if kwargs.get('latitud', None) is not None and kwargs.get('longitud', None) is not None:
            self.geolocalizacion = kwargs['latitud'] + ',' + kwargs['longitud']
        else:
            self.geolocalizacion = ''
        self.betado = kwargs.get('betado', False)

        self.created_at = datetime.datetime.now().strftime("%d/%m/%Y")

    def insert(self):
        if not DB.find_one('clientes', { "ci": self.ci }):
            DB.insert(collection='clientes', data=self.json())
        else:
            raise ValueError("Ya existe cliente con ci", str(self.ci))
    def json(self):
        return {
            'nombres': self.nombres,
            'ap_paterno': self.ap_paterno,
            'ap_materno': self.ap_materno,
            'ci': self.ci,
            'fecha_nacimiento': self.fecha_nacimiento,
            'email': self.email,
            'direccion': self.direccion,
            'geolocalizacion': self.geolocalizacion,
            'created_at': self.created_at,
            'betado': self.betado
        }
