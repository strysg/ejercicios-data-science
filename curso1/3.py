''' 
Analizando datos empresas con pandas y visualizando
'''
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt

# cargando el archivo csv en un dataFrame
dt = pd.read_csv('/home/strymsg/Descargas/Padron012019RegistroComercio2.csv', sep='\t')
#print (dt)

actividades = dt['ACTIVIDAD']
oA = actividades.value_counts()

# obteniendo array de actividades
oav = oA.values
# Las quince actividades con mas ocurrencias
oav = oav[0:15]
# agregando sumatoria de todos los demas
oav = np.append(oav, oA.values[:16].sum())

# etiquetas de las quince actividades con mas ocurrencias
oal = np.array(oA.axes)[0, 0:15]
# agregando etiqueta final
oal = np.append(oal, 'Otras')

# mostrando resultados en una torta, las primeras quince y el resto agrupadas
plt.pie(oav, labels=oal, autopct='%1.1f%%', shadow=True)
plt.show()

# oA es del tipo pandas.Series
# TODO: ver la forma de mostrar una grafica de las 50 actividades que mas se repiten.

