import pandas as pd
import numpy as np
rutaRios = '/home/strymsg/Documentos/MaestriaCienciaDatos/Curso1/Informes/Rios1.csv'
rutaLagos = '/home/strymsg/Documentos/MaestriaCienciaDatos/Curso1/Informes/Lagos1.csv'

# Rios
# Descripcion estadistica
pdr = pd.read_csv(rutaRios)

print('Estadisticas descriptivas de la longitud de los rios')
pdr.describe()

print('Gráfica de distribución de longitud de ríos en Km.')
plt.hist(pdr['longitud'], bins=pdr['longitud'].count(), normed=True, edgecolor='black')
# plt.savefig('foog.png')
plt.show()

# Lagos
pdl = pd.read_csv(rutaLagos)

# Sanitizando los datos
for i in pdl.index:
    if (abs(pdl.loc[i]['latitud']) > 900):
        pdl.set_value(i, 'latitud', pdl.loc[i]['latitud'] / 1000)
        print(pdl.loc[i]['latitud'])

for i in pdl.index:
    if (abs(pdl.loc[i]['longitud']) > 900):
        pdl.set_value(i, 'longitud', pdl.loc[i]['longitud'] / 1000)

# Descripcion estadistica
print('Estadísticas básicas de lagos')
pdl.describe()

# Grafica de superficie de lagos Vs elevacion aumentada en 100M

pdl['elev1000M'] = pdl[:]['elevacion'] * 100000000
pdl[:][['superficie', 'elev1000M']].plot()
plt.show()

# Gráfica de lagos en mapa con marca de color por el tamaño de la superfice

lat = pdl['latitud'].values
lon = pdl['longitud'].values
lagos = pdl['Lagoi'].values
superficies = pdl['superficie'].values

# ajustando proyeccion

# 1. ajustando el fondo del mapa
fig = plt.figure(figsize=(8, 8))

m = Basemap(projection='lcc', resolution='h', 
            lat_0=pdl['latitud'].mean(), lon_0=pdl['longitud'].mean(),
            width=1.1E6, height=1.2E6)

m.shadedrelief()
m.drawcoastlines(color='gray')
m.drawcountries(color='gray')
m.drawstates(color='gray')

# 2. Dibujando los puntos
m.scatter(lon, lat, latlon=True,
          c=np.log10(superficies), s=150,
          cmap='Reds', alpha=0.5)

# 3. colorbar
plt.colorbar(label=r'$\log_{10}({\rm superficie})$')
plt.clim(3, 7)

### Gráfica de lagos en mapa con marca de color por la elevación
elevaciones = pdl['elevacion'].values

# 1. ajustando el fondo del mapa
fig = plt.figure(figsize=(8, 8))

m = Basemap(projection='lcc', resolution='h', 
            lat_0=pdl['latitud'].mean(), lon_0=pdl['longitud'].mean(),
            width=1.1E6, height=1.2E6)

m.shadedrelief()
m.drawcoastlines(color='gray')
m.drawcountries(color='gray')
m.drawstates(color='gray')

# 2. Dibujando los puntos
m.scatter(lon, lat, latlon=True,
          c=np.log10(elevaciones), s=70,
          cmap='RdBu_r', alpha=0.35)

# 3. colorbar
plt.colorbar(label=r'elevación km snm')
plt.clim(1, 7)



## Analisis de datos demograficos

rutaCsv = '/home/strymsg/Documentos/MaestriaCienciaDatos/Curso1/Informes/indicadores_municipios_cpv-2012_cod.csv'

