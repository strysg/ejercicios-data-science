## Crear arays o matrices aleatorias
import matplotlib.pyplot as plt
import numpy as np


## espacio lineal ##

# crea un espacio lineal de 0 a 10 con 1000 elementos ...
np.linspace(0, 10, 1000)

## valores Aleatorios ##

# Carga a rng con un valor de inicio aleatorio 1999 seria como la semilla
rng = np.random.RandomState(1999)

# la funcion rand() crea un array n dimensional, con el numero de elementos dado

# array de cuatro elementos
rng.rand(4)

# dos dimensiones de 4 filas 5 columnas
rng.rand(4,5)

# asi sucesivamente
rng.rand(4,5 8,1)


