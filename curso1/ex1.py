#!/bin/python3
# datos de http://raw.githubusercontent.com/selva86/datasets/master/midwest_filter.csv
import pandas as pd
import numpy as np

# ruta = 'http://raw.githubusercontent.com/selva86/datasets/master/midwest_filter.csv'
ruta = 'midwest_filter.csv'
midwest = pd.read_csv(ruta)

############################## Obtener resumenes estadisticos de al menos 5 atributos

total = midwest.loc[:]['poptotal'].sum() # poblacion total

# 1. Cantidad total de poblacion negra y porcentaje con el total
popBlackTotal = midwest.loc[:]['popblack'].sum()
print('Porcentaje poblacion negra:', str((popBlackTotal * 100) / total) + '%')
input('presione tecla para continuar...')


# 2. Cantidad total de poblacion blanca y porcentaje con el total
popWhiteTotal = midwest.loc[:]['popwhite'].sum()
print('Porcentaje poblacion blanca:', str((popWhiteTotal * 100) / total) +'%' )
input('presione tecla para continuar...')

# 3. Porcentaje de poblacion adulta
popAdultsTotal = midwest.loc[:]['popadults'].sum()
print('Porcentaje poblacion adulta:', str((popAdultsTotal * 100) / total) +'%' )
input('presione tecla para continuar...')

# 4. Media de porcentaje de poblacion por debajo de la pobreza
pobreza = midwest.loc[:]['percbelowpoverty']
print('Media de porcentaje de poblacion pobre:', str(pobreza.mean()) + '%')
input('presione tecla para continuar...')

# 5. datos descriptivos de la poblacion que asiste al colegio
print('Poblacion que asiste a la universidad:', str(midwest.loc[:]['percollege'].describe()))
input('presione tecla para continuar...')

################################## Presentar al menos 5 graficas de analisis de los datos base
import matplotlib.pyplot as plt

# 1. Poblacion adulta vs poblacion total
kwargs = dict(histtype='stepfilled', alpha=0.3, normed=True, bins=100)

plt.hist(midwest.loc[:]['poptotal'], **kwargs)
plt.hist(midwest.loc[:]['popadults'], **kwargs)

plt.show()

# 2. Comparacion de poblaciones por etnias que no son blancas

midwest[:][['popblack','popasian','popamerindian','popother']].plot()
plt.show()

# kwargs = dict(histtype='stepfilled', alpha=0.3, normed=True, bins=20)
# plt.hist(midwest.loc[:]['popwhite'], **kwargs)
# plt.hist(midwest.loc[:]['popblack'], **kwargs)
# plt.hist(midwest.loc[:]['popamerindian'], **kwargs)
# plt.hist(midwest.loc[:]['popasian'], **kwargs)
# plt.hist(midwest.loc[:]['popother'], **kwargs)
plt.show()

# 3. 15 estados con la mayor cantidad de poblacion 
dt1 = midwest.loc[:, ['county', 'poptotal']]
ord1 = dt1.sort_values(by='poptotal', ascending=False)

values = ord1.iloc[:15]['poptotal'].values
labels = ord1.iloc[:15]['county'].values

plt.pie(values, labels=labels, autopct='%d%%', shadow=True)
plt.show()
