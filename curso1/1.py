# Craps

# Juego, tienes dos datos si sacas 7 u 11 en el primer lanzamiento ganas.
# si sacas 2,3 o 12 pierdes.
# si sacas cualquier otro número es tu punto.
# vas lanzando los datos hasta sacar tu punto y si sacas 7 pierdes.

# Hacer un programa para jugar claps.

# - Hacer un simulador de claps que haga diez millones de lanzamientos y calcular la probabilidad de ganar.
import random

def lanzar():
    dado1 = random.randint(1,6)
    dado2 = random.randint(1,6)
    return dado1 + dado2

def evaluar(valor, punto=None):
    ''' Simula lanzamiento de un dado, segun las reglas de craps.
    -- retorno: 'continua' --> si el juego continua
    -- retorno: 'gano'
    -- retorno: 'perdio'
    '''
    if punto:
        if valor == punto:
            return 'gano'
        elif valor == 7:
            return 'perdio'
        else:
            return 'continua'
    if valor == 2 or valor == 3 or valor == 12:
        return 'perdio'
    elif valor == 11 or valor == 7:
        return 'gano'
    else:
        return 'continua'

def jugar(mostrar=True):
    
    ''' Simula un juego entero mostrando o no lanzamientos
    -- retorna el numero de lanzamientos que se tomo para terminar el juego
       y si este se gano o perdio, ej: (<lanzamientos>, gano) , (12, true)
    -- (1, false)
    '''
    lanzamientos = 0
    l = lanzar()
    e = evaluar(l)
    if mostrar:
        print ('Primer lanzamiento:', l, '-->', e)
    if e == 'perdio':
        return (1, False)
    elif e == 'gano':
        return (1, True)
    
    lanzamientos += 1
    # punto
    p = l
    if mostrar:
        print (' punto:', l)
    e = evaluar(lanzar(), p)
    # continuando el juego
    while e != 'perdio' and e != 'gano':
        lanzamientos += 1
        l = lanzar()
        e = evaluar(l, p)
        if mostrar:
            print (' ', l, '->', e)

    if mostrar:
        print ('Fin del juego:', e)
    if e == 'perdio':
        return (lanzamientos, False)
    else:
        return (lanzamientos, True)

#res = jugar()
#print ('res', res)

def simulacion(veces=10000):
    ''' Ejecuta juegos un numero de `veces' dado y retorna los resultados
    --- return: (ganadas, perdidas, lanzamientos)
    '''
    ganadas = 0
    perdidas = 0
    lanzamientos = 0
    for i in range(veces):
        res = jugar(False)
        lanzamientos += res[0]
        if res[1] == True:
            ganadas += 1
        else:
            perdidas += 1
    return (ganadas, perdidas, lanzamientos)

# Ahora probando 50 veces
ganadas = 0
perdidas = 0
lanzamientos = 0
n = input('Introduzca el número de juegos: ')
nroJuegos = int(n)
n = input('Introduzca el número de simulaciones: ')
nroSimulaciones = int(n)

print ('Iniciando simulacion')
for i in range(nroSimulaciones):
    res = simulacion(nroJuegos)
    print ('simulacion', i,': ganadas', str((res[0]/nroJuegos)),', perdidas', str((res[1]/nroJuegos)),', lanzamientos', str(res[2]))
    ganadas += res[0]
    perdidas += res[1]
    lanzamientos += res[2]

print ('=======================================================')
print ('Simulaciones:', str(nroSimulaciones))
print (' Total juegos:', str(nroJuegos*nroSimulaciones))
print ('    Total lanzamientos:', lanzamientos)
print ('       ganadas:', ganadas, ' --> promedio:',
       str((ganadas/(nroJuegos*nroSimulaciones)) * 100))
print ('       perdidas:', perdidas, '--> perdidas:',
       str((perdidas/(nroJuegos*nroSimulaciones)) * 100))

