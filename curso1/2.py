## Juego el ahorcado en python
# Rodrigo Garcia Saenz - 2019
import random

def mostrarPalabra(palabra, letras):
    ''' dada una palabra y dadas las letras va mostrando en la pantalla
    las letras encontradas. Ej:
    palabra = concentrado
    letras = ['c','a']
       c _ _ c _ _ _ _ a _ _

    palabra = pensamiento
    letras = ['e', 'm', 'o', 't']
       _ e _ _ _ m _ _ _ t o

    -- Reotorna True si letras contiene todas las `letras' en `palabra'.
    '''
    contador = 0
    for c in palabra:
        if c in letras:
            print (c, end = ' ')
            contador += 1
        else:
            print ('_', end = ' ')
    if contador == len(palabra):
        return True
    return False

def mostrarGraficoAhorcado(restantes):
    if restantes == 0:
        print ('''
        - - 
          \\
           |
           |
           O
          /|\ 
           |
          / \
        ''')
    elif restantes == 1:
        print ('''
        - - 
          \\
           |
           |
           O
          /|\\ 
           |
          / 
        ''')
    elif restantes == 2:
        print ('''
        - - 
          \\
           |
           |
           O
          /|\\ 
           |

        ''')
    elif restantes == 3:
        print ('''
        - - 
           \\
           |
           |
           O
          /|\\ 


        ''')
    elif restantes == 4:
        print ('''
        - - 
           \\
           |
           |
           O
          / \\ 


        ''')
    elif restantes == 5:
        print ('''
        - - 
           \\
           |
           |
           O
            \\ 


        ''')
    elif restantes == 6:
        print ('''
        - - 
           \\
           |
           |
           O



        ''')
    elif restantes == 7:
        print ('''
        - - 
           \\
           |
           |




        ''')
    elif restantes == 8:
        print ('''
        - - 
           \\
           |
           
           
           


        ''')
    elif restantes == 9:
        print ('''
        - - 
           \\
           
           
           
            


        ''')
    elif restantes == 10:
        print ('''
        - - 
           
           
           
           
            


        ''')        
    elif restantes == 11:
        print ('''
        -  
           
           
           
           
            


        ''')        
# programa principal
palabras = ['juegos', 'programación', 'python', 'neuronas', 'análisis', 'datos', 'procesamiento', 'pensamiento', 'sugerencia',
            'computadora', 'convulcional', 'procedimiento', 'comercio', 'inteligencia', 'artificial', 'aprendizaje']

palabra = palabras[random.randint(0, len(palabras) - 1)]
restantes = 11
ganado = False
letras = []
letras.append(palabra[random.randint(0, len(palabra) - 1)]) # primera letra ayuda

while restantes >= 0 and ganado == False:
    print ('Intentos restantes:', restantes)
    print ()
    mostrarGraficoAhorcado(restantes)
    ganado = mostrarPalabra(palabra, letras)
    print ()
    letra = input('Introduzca una letra: ')
    letras.append(letra)
    restantes -= 1

if ganado == True:
    print ('Juego terminado, ¡has ganado!')
else:
    print ('Juego terminado. Perdiste la palabra era "', palabra, '"')
    mostrarGraficoAhorcado(restantes)
