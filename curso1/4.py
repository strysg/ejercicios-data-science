# muestra con datos financieros

import numpy as np
import pandas as pd
from pandas_datareader import data

import datetime


goog = data.DataReader('GOOG', start='2004', end='2016', data_source='yahoo')
goog.head()

goog = goog['Close']

import matplotlib.pyplot as plt

# muestra basica
print('Muestra basica')
goog.plot()
plt.show()

goog.plot(alpha=0.5, style='-')
goog.resample('BA').mean().plot(style=':')
goog.asfreq('BA').plot(style='--');
plt.legend(['input', 'resample', 'asfreq'], loc='upper left')
print('Muesta usando resample y asfreq')
plt.show()
