# Uses open-elevation api to get elevation data given lat and long from csv file

#datasets/
OPEN_ELEVATION_INSTANCE='http://rmgss.net:10000'
API_LOOKUP= '/api/v1/lookup?locations='

# curl http://rmgss.net:10000/api/v1/lookup\?locations=\-16.58085,-68.15566

import csv
import requests

csvHeaders = ['Latitude', 'Longitude', 'Elevation']

def getElevation(latitude, longitude):
    ''' Do http request to get elevation
    # ex: http://rmgss.net:10000/api/v1/lookup?locations=-16.58085,-68.15566
    '''
    lt = ''
    lg = ''
    prefix = ''
    if latitude.endswith('S'):
        prefix = '-'
    lt = prefix + latitude[:-1]
    if longitude.endswith('W'):
        prefix = '-'
    lg = prefix + longitude[:-1]
        
    try:
        # print(':::', OPEN_ELEVATION_INSTANCE + API_LOOKUP + lt + ',' + lg)
        r = requests.get(OPEN_ELEVATION_INSTANCE + API_LOOKUP + lt + ',' + lg)
        # expected response:
        # {'results': [{'elevation': 3742, 'latitude': -16.58085, 'longitude': -68.15566}]}
        elevation = r.json()['results'][0]['elevation']
        return elevation
    except Exception as E:
        print('Error in ', str(lg), ',', str(lg),':', str(E))
        raise E

# abriendo para escribir un nuevo csv
with open('elevationsByCity.csv', 'w') as csvfilewrite:
    writer = csv.DictWriter(csvfilewrite, fieldnames=csvHeaders)
    writer.writeheader()
    # leyendo el archivo csv
    with open('GlobalLandTemperaturesByCity.csv', 'r') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',')
        for i, row in enumerate(csvreader):
            try:
                elevation = getElevation(row[5], row[6])
                print(i, elevation)
                # escribiendo
                try:
                    writer.writerow({
                        'Latitude': row[5],
                        'Longitude': row[6],
                        'Elevation': elevation
                    })
                    if i%500 == 0:
                        print('Done:', i, row[5], row[6], elevation)
                except Exception as E:
                    print(i,':', str(E))
                    writer.writerow({
                        'Latitude': row[5],
                        'Longitude': row[6],
                        'Elevation': -5555 # marca
                    })            
            except Exception as E:
                print('Error en fila', i)
                # escribiendo
                writer.writerow({
                    'Latitude': row[5],
                    'Longitude': row[6],
                    'Elevation': -5555 # marca error
                })
print('Done, check output')

            
