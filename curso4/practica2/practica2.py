#!/usr/bin/env python
# coding: utf-8

# ## ROSSMAN
# 
# Estudiante: Rodrigo Mauricio Garcia Saenz
# 
# ### 1. Quitar filas duplicadas
# 

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from pandas import Series, DataFrame
from pandas.plotting import autocorrelation_plot
import seaborn as sb
import os
dtset = pd.read_csv('RossmanDataV1.csv')
dtset.head(10)


# In[2]:


dtset.drop_duplicates(subset=None, inplace=True)


# ### 2. Quitar columnas duplicadas 

# In[3]:


# Eliminar las columnas duplicadas
def getDuplicateColumns(df):
    duplicateColumnNames = set()
    # Iterate over all the columns in dataframe
    for x in range(df.shape[1]):
        # Select column at xth index.
        col = df.iloc[:, x]
        # Iterate over all the columns in DataFrame from (x+1)th index till end
        for y in range(x + 1, df.shape[1]):
            # Select column at yth index.
            otherCol = df.iloc[:, y]
            # Check if two columns at x 7 y index are equal
            if col.equals(otherCol):
                duplicateColumnNames.add(df.columns.values[y])
    return list(duplicateColumnNames)


# In[4]:


duplicateColumnNames = getDuplicateColumns(dtset)
print('Duplicate Columns are as follows')
for col in duplicateColumnNames:
    print('Column name : ', col)


# In[5]:


newDf = dtset.drop(columns=getDuplicateColumns(dtset))
newDf.head(1)


# In[ ]:





# ### 3. Identificar columnas que tienen valores constantes

# In[6]:


def get_constant_columns(dataframe):
    constantColumns = set()
    for column in dataframe.columns:
        if len(dataframe[column].unique()) == 1: # un solo valor unico
            # print(dataframe[column])
            constantColumns.add(column)
    return constantColumns


# ### 4. Identifique el porcentaje de valores faltantes en las columnas

# In[7]:


# 4. Identifique el porcentaje de valores faltantes en las columnas
total = newDf.isnull().sum().sort_values(ascending=False)
percent = (newDf.isnull().sum()/newDf.isnull().count()).sort_values(ascending=False)
missing_data = pd.concat([total, percent], axis=1, keys=['Total', 'Percent'])
missing_data


# ### 5. Analice el caso de los atributos por medio de gráficas Cuales decidiría usted de transformar o en su caso quitar, con el propósito de mejorar el dataset.
# 
# Genere las siguientes gráficas:
# 
#   a. Histograms
#   
#   b. Quartiles
#   
#   c. Scatter plots
#   
#   d. Distributions
#   
#   e. Heartmaps

# In[8]:


# a. Histograms
newDf.columns.values
newDf.hist(figsize=(16, 20)) #, xlabelsize=8, ylabelsize=8)


# En las gráficas de histogramas la columna "CompetitionOpenSinceYear" no tiene cambios, por lo que conviene quitarla.

# In[9]:


#   b. Quartiles
# (usando boxplot)
colsNumericas = newDf.select_dtypes(include=[np.number])
for col in colsNumericas:
    sb.boxplot(x=col,data=newDf)
    plt.show()


# In[10]:


# c. Scatter plots
print("----------------- Scatter plot con seaborn -----------")
x = sb.PairGrid(newDf)
x = x.map(plt.scatter)


# In[11]:


newDf['CompetitionOpenSinceYear'].unique()


# In[12]:


# Gráficos de distribucion
for col in colsNumericas:
    if missing_data.loc[col]['Total'] == 0:
        print(col)
        sb.distplot(newDf[col])
        plt.show()
    # else: tal vez realizar un ajuste para darle valor numerico


# In[13]:


# Heartmaps
corrmat = newDf.corr()
f, ax = plt.subplots(figsize=(10, 7))
sb.heatmap(corrmat, vmax=.8, square=True);


# ### 6. Obtenga la matriz de correlación de las variables numéricas e identifique si algunas se pudieran quitar.

# In[14]:


numeric_columns = newDf.select_dtypes(include=[np.number])
numeric_columns.describe()


# In[15]:


df_numerics_only = dtset.select_dtypes(include=[np.number])
df_numerics_only.head()

correlated_features = set()  
correlation_matrix = df_numerics_only.corr()

for i in range(len(correlation_matrix.columns)):  
    for j in range(i):
        if abs(correlation_matrix.iloc[i, j]) > 0.8: # variables con alta correlacion
            colname = correlation_matrix.columns[i]
            correlated_features.add(colname)

print( len(correlated_features) )

print(correlated_features)

df_prueba = dtset
df_prueba.drop(labels=correlated_features, axis=1, inplace=True)

df_prueba.head(20)


# ### 7. El atributo Competition distance tiene 9999 como null, cambie este valor por el que considere se adecua más a este campo (i.e. media, mediana, algún otro)

# In[16]:


dtset['CompetitionDistance'][dtset['CompetitionDistance'] > 9999].count()
cv = dtset.copy()
dtset['CompetitionDistance'][dtset['CompetitionDistance'] == 9999].count()
cv['CompetitionDistance'][cv['CompetitionDistance'] == 9999].map({ 9999: cv['CompetitionDistance'].median() })
cv['CompetitionDistance'][cv['CompetitionDistance'] == cv['CompetitionDistance'].median()]


# ### 8. Mapear Day of Week con los días en literal.

# In[17]:


cv['DayOfWeek'].unique()
dias = { 
    1: 'Domingo',
    2: 'Lunes',
    3: 'Martes',
    4: 'Miercoles',
    5: 'Jueves',
    6: 'Viernes',
    7: 'Sábado'
}
cv['DayOfWeek'].map(dias)


# ### 10. Mapear el atributo CompetitionOpenSinceMonth con los meses del año en literal y rellenar los valores faltantes “?” con el mes que tenga la mayor frecuencia.

# In[18]:


dtset['CompetitionOpenSinceMonth'].unique()
# Obteniendo el mes con mayor frecuenca
dtset['CompetitionOpenSinceMonth'].describe()
sb.boxplot(x='CompetitionOpenSinceMonth',data=dtset)
dic = {}
for mes in dtset['CompetitionOpenSinceMonth'].unique():
    dic[mes] = dtset['CompetitionOpenSinceMonth'][dtset['CompetitionOpenSinceMonth'] == mes].count()
n = 0
mesMasFrecuencia = 0
for k,v in dic.items():
    if v > n:
        n = v
        mesMasFrecuencia = k
print('Mes con mas frecuencia', mesMasFrecuencia,'con', n)
meses = { 
    0.: 'Enero',
    1.: 'Febrero',
    2.: 'Marzo',
    3.: 'Abril',
    4.: 'Mayo',
    5.: 'Junio',
    6.: 'Julio',
    7.: 'Agosto',
    8.: 'Septiembre',
    9.: 'Octubre',
    10.: 'Noviembre',
    11.: 'Diciembre'
}
cv = dtset.copy()
cv['CompetitionOpenSinceMonth'].replace(np.NaN, mesMasFrecuencia, inplace=True)
#cv['CompetitionOpenSinceMonth'].unique()
cv['CompetitionOpenSinceMonth'] = cv['CompetitionOpenSinceMonth'].map(meses)
print(cv['CompetitionOpenSinceMonth'].head(10))
print(cv['CompetitionOpenSinceMonth'].tail(10))


# ### 11. Identifique atributos que no serían de utilidad por su nivel de variación, si hubiera. Usted identifique su nivel de utilidad, dando razones para ello.

# In[21]:


df_numerics_only = cv.select_dtypes(include=[np.number])
df_numerics_only.describe()
#for col in df_numerics_only.columns:
#    print(col)
#    print


# In[53]:


df_numerics_only = cv.select_dtypes(include=[np.number])
# analizando las columnas por su "bajo nivel de variacion"
columnas_a_eliminar = []
for col in df_numerics_only.columns:
    print(col)
    print(cv[col].describe())
    #print(cv[col].quantile(.25), cv[col].quantile(.50), cv[col].quantile(.75))
    try:
        sb.distplot(df_numerics_only[col])
        plt.show()
    except Exception as e:
        print('No se pudo graficar', e)
    # usando el criterio de la distribucion de los cuantiles .25 .5 y .75 es igual
    # significa que los datos son mayormente del mismo valor y casi no hay variacion
    if cv[col].quantile(.25) == cv[col].quantile(.5)  and cv[col].quantile(.5) == cv[col].quantile(.75):
        columnas_a_eliminar.append(col)
        print('✕✕✕✕✕✕✕✕✕✕✕✕✕✕✕✕✕✕✕✕✕✕✕✕✕✕✕✕✕✕✕✕✕✕✕✕')
        print(' Se elimina la columna:', col)
        print('Cuantiles .25 .5 .75:', cv[col].quantile(.25), cv[col].quantile(.50), cv[col].quantile(.75))
        print()


# ### 12. Identifique los campos que tienen valores faltantes y decida como reemplazarlos, haciendo un análisis en cada caso.
# 

# In[70]:


cv['CompetitionOpenSinceYear'][cv['CompetitionOpenSinceYear'].isnull()]

columnas_con_faltantes = []
# identificando columnas con valores faltantes
for col in df_numerics_only.columns:
    if cv[col].isnull().any():
        columnas_con_faltantes.append(col)
columnas_con_faltantes
# analizando cada caso
for col in columnas_con_faltantes:
    print()
    print(col,'****', 'valores unicos:',len(cv[col].unique()))
    print(' Valores nulos:', len(cv[col][cv[col].isnull()]))
    print(cv[col].describe())
    # rellenando con la media en caso de que la desviacion estandar sea baja
    if ((cv[col].std()*100)/(cv[col].mean())<= 0.15):
        cv[col].replace(np.NaN, cv[col].mean(), inplace=True)
        print(' -> Reemplazando Faltantes por la media:', cv[col].mean())     

