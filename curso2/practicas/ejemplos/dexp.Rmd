---
title: "Diseño de experimento"
author: "Rodrigo Garcia"
date: "5/10/2019"
output: html_document
---

## Ejericcios de diseño de experimento

El departamento de ... piensa que la resistencia del madera dura es una función de la pulpa y que el 
rango de las concenctaciones de madera dura de interés práctico está entre 5 y 20%. El equipo
de ingenieros responsable del estudio decide investigar cuatro niveles de concentración de madera
dura. Por ejemplo: 5%, 10%, 15% y 20%. Deciden hacer 6 ejemplares de prueba con cada nivel de 
concentración, utilizando una planta piloto. Los datos son los siguientes.

Formulación de hipótesis:

$$
H_{0} = \mu_{0} = \mu_{1} = \mu_{2} = \mu_{3} = \mu_{4}\\
H_{1} = \mu_{i} \neq \mu_{j}
$$
Las medias $\mu_{i}$ se refieren a la resistencia de acuerdo al nivel de concentración de madera dura en la.

| Concentración de madera dura |  observaciones |
|---|---|

|    | 1 | 2 | 3 | 4 | 5 | 6 | 
|----|--|--|----|---|---|--|---------------|
| 5% |7 |8|18|11|9|10 |
| 10% | 12 | 17 | 13 | 18 | 19 | 15 |
| 15% | 14 | 18 | 19 | 17 | 16 | 18 |
| 20% | 19 | 25 | 22 | 23 | 18 | 20 |

El modelo estadístico de un diseño experimental de un factor, se presenta de la siguiente manera:

$$
y_{ij} = \mu + \tau_{i} + \epsilon_{ij}\\
i: ésimo\ tratamiento\\
$$
Así reescribiendo la tabla para calcular

| Concentración de madera dura |  observaciones |
|---|---|

|    | 1 | 2 | 3 | 4 | 5 | 6 | $y_{i}$| $\overline{y_{i}}$ |
|----|--|--|----|---|---|--|---------------|---|---|
| 5% | 7 |8  |15 |11  |9  |10 | 60 | 10|
| 10% | 12 | 17 | 13 | 18 | 19 | 15 | 94 | 15.67 |
| 15% | 14 | 18 | 19 | 17 | 16 | 18 |  102| 17.00|
| 20% | 19 | 25 | 22 | 23 | 18 | 20 | 127| 21.17|

Para toda la resolución de esta tabla se usa: ANOVA o análisis de varianza. Ver siguiente ejemplo.

### Ejemplo 2 

Tiempo de coagulación sangúinea.

Los siguientes datos muestran los tiempos de coagulación para muestras de sangre extraídas de 24 animales que recibieron cuatro dietas diferentes A, B, C y D. 

Los animales fuerón asignados aleatoriamente a las dietas y las muestras de sangre fuerón tomadas y analizadas en orden aleatorio. ¿Hay evidencia que indique una diferencia real entre los tiempos medios de coagulación para las cuatro diferentes dietas?

#### sol.

Planteando:

$$
H_{0}: \mu_{A} = \mu_{B} = \mu_{C} = \mu_{D}\\
H_{1}: \mu_{i} \neq \mu_{j}
$$

```{r}
coagulation = read.table('./BASEBLOOD3.txt', header=TRUE);
coagulation;
plot(coag ~ diet, data=coagulation)
# la siguientes líneas realizan el ANOVA
g = lm(coag ~ diet, coagulation)  ## realiza la estimación del modelo 
# resumen
summary(g)
```
 - En este caso `coag` es la variable dependeiente  y `diet` es la variable independiente.
 - El F calculado es 13.57 con 20 grados de libertad.
 - el p-value: 4.658e-05, como este valor tiende a 0. Así si el p-value es menor a $\alpha$, es decir: p-value < $\alpha$. Por tanto se rechaza la hipótesis nula $H_{0}$ y se acpeta la hipótesis alternativa.
 
 #### Conclusión
 
 La hipótesis nula plantea que el tiempo de coagulación promedio de la dieta A es igual al de la dieta B, C y D , por el experimento realizado se rechaza $H_{0}$ y se acepta la hipótesis alternativa que indica que hay diferencia real entre los tiempos de coagulación promedio entre las diferentes dietas. 
 
 Otra forma alternativa de probar es usando la prueba t-student comparando cada media de cada población.
 
 ----
 Resolviendo el ejercicio anterior. (pendiente)
 p
 

