---
title: "Práctica 2"
output: html_notebook
---

### Combinaciones
#### 1. De cúantas formas pueden elegirse simultáneamente tres bolas de una urna en la que hay al menos tres bolas blances y tres negras indistinguibles

- n: 3 (por que el grupo contiene 3 bolas)
- p: 2 (por qué hay dos subgrupos, uno de blancas y otro de negras)

No importa el orden de las bolas y se usan combinaciones con repetición por que se pueden repetir grupos:

```{r}
factorial(4)/(factorial(3)*factorial(1))
```

#### 2. Las diagonales de un polígono se obtienen uniendo pares de vértices no adyacentes. Obtener el número de diagonales del cuadrado y el hexágono.

- n: 4; Número de vértices
- p: 2; cada grupo es de dos vértices

No importa el orden:

```{r}
choose(4,2)
```

Da 6 uniiones posibles entre vértices, pero sólo debemos contar las uniones entre vértices no adyacentes, en un cuadrado hay un total de 4 vértices adyacentes, entonces: 6 - 4 = **2 diagonales posibles**

Para el hexágono:

- n: 6; Número de vértices
- p: 2; Cada grupo es de dos vértices

```{r}
choose(6,2)
```

Da un resultado de 15 - 6 = **9 diagonales**, es por que hay un total de 15 uniones entre vértices y hay 6 vértices adyacentes.

#### 3. Cuatro libros de matemáticas, seis de física y dos de química han de ser colocados en un estantería ¿cúantas colocaciones distintas admiten en si:

1. Los libros de cada materia han de estar juntos.

Como en cada grupo deben estar los libros de cada materia

- 3! permutaciones posibles entre materias
- 6! permutaciones posibles libros de física
- 4! permutaciones libros de matemáticas
- 2! permutaciones libros de qumica

```{r}
factorial(3) * factorial(6) * factorial(4) * factorial(2)
```

Finalmente 3! + 6!+ 4!+ 2! = **207360 colocaciones distintas**

2. Sólo los de matemáticas tienen que estar juntos

- 4!: Los libros de matemáticas
- Quedan 1 permutación por los libros de matemáticas, 6 por los de física y 2 por los de química: 1! + 6! +2! = 9!

```{r}
factorial(9)*factorial(4)
```

Hay 8709120 fomas posibles.

### Variable aleatorio discreta

#### 1. Un embarque de 20 computadoras portátiles similares para una tienda minosrista contiene 3 que están defectuosas. Si una escuela compra al azar 2 de estas computadoras, calcule la distribución de probabilidad para el número de computadoras defectuosas.

La función de probabilidad es:

x: computadoras defectuosas encontradas.

f(x=0) , f(x=1) , f(x=2), entonces:

f(x=0):

```{r}
Px0 = (choose(3, 0) + choose(17,2)) / choose(20, 2) 
Px1 = (choose(3, 1) + choose(17,1)) / choose(20, 2) 
Px2 = (choose(3, 2) + choose(17,0)) / choose(20, 2) 
```

Donde:

 - choose(3,0): probabiliad de obtener 0 de las 3 defectuosas
 - choose(2,17): probabilidad de obtener 2 no defectuosas de las 17 no defectuosas.
 - choose(20,2): probablidad de obtener 2 de las 20 totales
 
De igual forma para f(x=1), f(x=2):

```{r}
c(Px0, Px1, Px2)
```

Finalmente la función de probabilidad de obtener defectuosas:

| x | 0 | 1 | 2 |
|---|---|---|---|
| f(x) | 0.72105263 | 0.10526316 |  0.02105263 |

#### 2. Si una agencia automotriz vende 50% de su inventario de cierto vehículo extranjero equipado con bolsas de aire laterales, calcule la fórmula para la distribución de probabilidad del número de automóviles con bolsas de aire laterales entre los siguientes 4 vehículos que venda la agencia.

- La probalidad de vender un automovíl con bolsas de aire laterales es 0.5
- El espacio muestral dado A = con bolsa de aire y N sin bolsa es de la forma; S = {AAAA, AAAN, ..., NNNN} un total de 16 posiblidades

```{r}
c(choose(4,0), choose(4,1), choose(4,2), choose(4,3), choose(4,0)) / 16
```

Distribución de probabiidad:

| x | 0 | 1 | 2 | 3 | 4 |
|---|---|---|---|---|---|
|f(x)|0.0625|0.25|0.375|0.25|0.0625|

Su fórmula: f(x) = c(4,x)* 1/16 , para x=0,1,2,3,4.

### Variable aleatorio continua

La longitud de ciertos tornillos en centímetros es una variable aleatoria con la siguiente función de densidad.

[f1.jpg](f1.jpg)

#### 1. Para hacer cierto trabajo se prefieren tornillos con longitud entre 1,7 cm y 2,4 cm. ¿Cuál es la probabilidad de que un tornillo tenga dicha longitud?

Calculando la probabilidad P(1.7 < X < 2.4) como el área bajo la curva de densidad entre x = 1.7  y x = 2.4: 

[f2.jpg](f2.jpg)

Graficando la curva de densidad mostrando el área comprendida entre x=1.7 y x=2.4:

```
f = function(x) {
  y = (3/4)*(4*x - x*x - 3);
  return(y);
};

vals = seq(from=1.7, to=2.4, by=0.05);
y = sapply(vals1, f);

plot(vals, y)
```
```{r}
f = function(x) {
  y = (3/4)*(4*x - x*x - 3);
  return(y);
};

vals = seq(from=1.7, to=2.4, by=0.05);
y = sapply(vals1, f);

plot(vals, y)
```

#### 2. 

### Distribución binomial

#### La probabilidad de que un paciente se recupere de una rara enfermedad sanguínea es de 0.4. Si se sabe que 15 personas contrajeron la enfermedad, ¿cúal es la probabilidad de que a) sobrevivan al menos 10, b) sobrevivan 3 a 8, c) sobrevivan exactamente 5?

a) Se calcula la probabilidad de X >= 10 que es 1 - P(X < 10), aplicando la función de distribución de probabilidad binomial para valores de x de 0 a 9:

```
?pbinom
pa = 0
pa = pa + dbinom(0, size=15, prob=0.4)
pa = pa + dbinom(1, size=15, prob=0.4)
pa = pa + dbinom(2, size=15, prob=0.4)
pa = pa + dbinom(3, size=15, prob=0.4)
pa = pa + dbinom(4, size=15, prob=0.4)
pa = pa + dbinom(5, size=15, prob=0.4)
pa = pa + dbinom(6, size=15, prob=0.4)
pa = pa + dbinom(7, size=15, prob=0.4)
pa = pa + dbinom(8, size=15, prob=0.4)
pa = pa + dbinom(9, size=15, prob=0.4)
1 - pa
```

```{r}
pa = 0
pa = pa + dbinom(0, size=15, prob=0.4)
pa = pa + dbinom(1, size=15, prob=0.4)
pa = pa + dbinom(2, size=15, prob=0.4)
pa = pa + dbinom(3, size=15, prob=0.4)
pa = pa + dbinom(4, size=15, prob=0.4)
pa = pa + dbinom(5, size=15, prob=0.4)
pa = pa + dbinom(6, size=15, prob=0.4)
pa = pa + dbinom(7, size=15, prob=0.4)
pa = pa + dbinom(8, size=15, prob=0.4)
pa = pa + dbinom(9, size=15, prob=0.4)
1 - pa
```

#### 2. La vida útil, en días, para frascos de cierta medicina de prescripción es una variable aleatoria que tiene la siguiente función de densidad:

[!f3](f3.png)

Calcule la probabilidad de que un frasco de esta medicina tenga una vida útil de a) al menos 200 días, b) cualquier lapso entre 80 y 120 días.

[!f4](f4.png)

```
f = function (x) {
  y = 20000/(x+ 100)^3;
  return(y);
}
integrate(f, 200, Inf)
```

```{r}
f = function (x) {
  y = 20000/(x+ 100)^3;
  return(y);
}
integrate(f, 200, Inf)
```

b) Solución:

[!f5](f5.png)

```{r}
integrate(f, 80, 200)
```