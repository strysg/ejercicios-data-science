---
title: "Proyecto Análisis estadístico I"
author: "Rodrigo Garcia"
date: "6/10/2019"
output:
  html_document:
    df_print: paged
---

Para este proyecto se usa un registro de mas de 400000 comercios ficticios.

## Columnas

- Actividades: Nombres ficticios de actividades
- Departamento: Nombres de departamentos de Bolivia al que pertenecen las actividades.
- Municipio: Nombres de municipios de Bolivia
- Tipo Societario: Tipo de sociedad usada del comercio que realiza la actividad.

#### Cargando datos

```{r}
datos = read.csv("./datos.csv", sep=",");
head(datos, n = 10)
length(datos$actividad)
#sort(dt$actividad)
#table(dt$actividad)
#dt1 = split(dt)

```

#### Mostrando gráficas características básicas

Por departamento.

```{r}
# conteo
dptos = table(datos$departamento)
colors = rainbow(length(dptos))
{
  plot(dptos, xlab = "Departamentos", 
       main="Registros de comercio según departamentos",
       ylab = "Registros comercio", col=colors);
  legend("topleft", legend=names(dptos), cex=0.6, text.col=colors);
}
```

```{r}
tipos = table(datos$tipo.societario)
colors = rainbow(length(tipos))
{
  plot(tipos, xlab = "Tipo societario", ylab = "Registros comercio", 
       main="Registros de comercio según tipo societario",
       col=colors);
  legend("topright", legend=names(tipos), cex=0.7, text.col=colors);
}
```

Municipios

```{r}
municipios = table(datos$municipio)
municipios = sort(municipios, decreasing = TRUE)
{
plot(municipios[1:20], xlab="20 Municipios con más comercios", ylab = "Núm Comercios");
legend('topright', legend=names(municipios[1:20]),  lty=1:20, cex=0.6, bg='lightblue')
}
```

### Cálculo de probabilidades

En el conjunto de datos se tiene una columna que indica el tipo de actividad que realiza el comercio.

```{r}
length(unique(datos$actividad))
```

En el conjunto de datos existen 99 diferentes actividades y a continuación para facilitar los cálculos y estimaciones se obtendrá el porcentaje de ocurrencia de cada actividad en una tabla.

```{r}
actividades = table(datos$actividad)
actividades = sort(actividades, decreasing = TRUE);
l = length(datos$actividad);
# se guardan los porcentajes de ocurrencia por cada actividad de comercio
actividades$prob = (actividades/l)
```
Con estos datos se puede hacer una estimación obteniendo muestras de distintos tamaños y probando las distintas probabilidades.

Obteniendo 100 veces una muestra de tamaño uno y calcular la probabilidad de que 16 actividades sean del tipo "RQ7".

```{r}
prob = actividades$prob["RQ7"]
dbinom(16, 100, prob)
```

La probabilidad de que al tomar 100 pruebas y que 16 sean actividades del tipo "RQ7", es **0.07795595**.

Se pueden hacer más pruebas similares sobre probabildades de muestras.

```{r}
# prob para P(LK6=180) para 250 pruebas
p = dbinom(180, 250, actividades$prob["LK6"])
cat("La probabilidad de que LK6", actividades$prob["LK6"], "ocurra 100 veces en 250 pruebas es", p, "\n")
# prob para P(AB5<42) para 250 pruebas
p = pbinom(42, 250, actividades$prob["AB5"], lower.tail = TRUE)
cat("La probabilidad de que AB5", actividades$prob["AB5"], "ocurra menos de 42 veces en 250 pruebas es", p, "\n")
# P(TU9 > 200) para 1500 pruebas
p = pbinom(200, 1500, actividades$prob["TU9"], lower.tail = FALSE)
cat("La probabilidad de que TU9", actividades$prob["TU9"], "ocurra mas de 200 veces en 1500 pruebas es", p, "\n")
# P(TU9 > 100) para 5000 pruebas
p = pbinom(100, 500, actividades$prob["QP6"], lower.tail = FALSE)
cat("La probabilidad de que QP6", actividades$prob["QP6"], "ocurra mas de 100 veces en 500 pruebas es", p, "\n")
# P(TU9 < 100) para 100000 pruebas
p = pbinom(5, 100000, actividades$prob["QP6"], lower.tail = TRUE)
cat("La probabilidad de que QP6", actividades$prob["QP6"], "ocurra menos de 5 veces en 100000 pruebas es", p, "\n")
```
Ahora calcularemos la probabilidad de ocurrencia por departamentos, municipios y tipo societario.

```{r}
l = length(datos$departamento)
municipios = table(datos$municipio)
municipios$prob = municipios/l
dptos = table(datos$departamento);
dptos$prob = dptos/l;
tipos = table(datos$tipo.societario);
tipos$prob = tipos/l;
```
Con estas probabilidades calculadas se puede ahora calcular probabilidades condicionales.


```{r}
p1 = table(datos$actividad == "RQ7" & datos$departamento == "SANTA CRUZ")
cat("Probabilidad de que sea la actividad RQ7 dado que es del departamento SANTA CRUZ:",p1['TRUE']/l," \n")
p2 = table(datos$actividad == "UV5" & datos$departamento == "LA PAZ")
cat("Probabilidad de que sea la actividad UV5 dado que es del departamento LA PAZ:", p2['TRUE']/l , "\n")
p3 = table(datos$actividad == "UV5" & datos$departamento == "CHUQUISACA")
cat("Probabilidad de que sea la actividad UV5 dado que es del departamento CHUQUISACA", p3['TRUE']/l , "\n")
p4 = table(datos$actividad == "UT5" & datos$departamento == "COCHABAMBA")
cat("Probabilidad de que sea la actividad UT5 dado que es del departamento de COCHABAMBA", p4['TRUE']/l, "\n")
p5 = table(datos$actividad == "AB0" & datos$departamento == "ORURO")
cat("Probabilidad de que sea la actividad AB0 dado que es del departamento de ORURO", p5['TRUE']/l, "\n")
p6 = table(datos$actividad == "EF9" & datos$departamento == "TARIJA")
cat("Probabilidad de que sea la actividad EF9 dado que es del departamento de TARIJA", p6['TRUE']/l, "\n")
```

### Pruebas de independencia

Ahora se puede hacer un análisis de las probabilidades de actividades y de tipos societario de los comercios, en este caso se va a probar si existe relación entre un tipo societario y una o más actividades.

$$
H_{0}: Las\ actividades\ y \ tipos \ de\ societarios \ son\ independientes.\\
H_{1}: Las\ actividades\ y \ tipos\ de \ societarios \ son\ dependientes.
$$

```{r}
rel = data.frame(datos$actividad, datos$tipo.societario)
rel = table(datos$actividad, datos$tipo.societario)
chisq.test(rel)
```
El resultado muestra un p-value de 2.2e-16 que es un valor muy bajo por debajo de un nivel de significacia de 0.01. Lo que nos indica muy poca evidencia para aceptar de la hipótesis nula H0, es decir se acepta que las variables actividades y tipos de societarios son **dependientes**.

De igual manera probamos el nivel de dependencia entre actividades con municipios y departamentos. 

```{r}
rel = data.frame(datos$actividad, datos$departamento)
rel = table(datos$actividad, datos$departamento)
chisq.test(rel)
rel = data.frame(datos$actividad, datos$municipio)
rel = table(datos$actividad, datos$municipio)
chisq.test(rel)
```
Que en ambos casos muestra un valor p muy bajo indicando que los comercios en sus variables *actividades* son dependientes de *municipios* o *deparatamentos*.

#### Relaciones

Se puede obtener un resumen que muestre la relación entre actividades y departamentos, por ejemplo en una tabla que indique cúantas actividades con nombre "AB0" existen en cada departamento:

```{r}
# Obteniendo la matriz de contingencia entre actividades y departamentos
tab1 = table(datos$actividad, datos$departamento)
tab1[1,] # indice 1 corresponde a la actividad "AB0"
``` 

Sin embargo la actividad "AB0" no es necesariamente una de las actividades con mayor frecuencia. Ahora se mostrará una matriz con las 10 actividades con mayor frecuencia de ocurrencia. Para obtener las 10 actividades con mayor frecuencia es necesario ordenar la matriz y para eso se require un criterio de ordenación.

El criterio de ordenación será el total de actividades, es decir la sumatoria de las actividades en los nueve departamentos. 

```{r}
# Agregando la columna "sum" que representa el total de actividades
tab1o = addmargins(A=tab1[,2:10], FUN=sum)
# se ordena usando el criterio "sumatoria" que es la columna 10
tab1Ord = tab1o[order(tab1o[,10], decreasing = T),]
#tab1Ord[1,2:10]
af1 = tab1Ord[2:11,1:9]
# se usa tab1Ord[2:11,2:10] para indicar las primeras 10 filas y se descartan la columna 1 (nulos) y la 10 (sumatoria creada para ordenar)
cat("Las diez actividades con mas frecuencia:",names(tab1Ord[1,1:9]),"\n")
af1
plot(af1, las=2, main="Distribución de las 10 actividades con mayor frecuencia \n en los 9 departamentos")
```

También se puede mostrar una gráfica básica de la matriz de correlación de las 10 actividades con mayor ocurrencia. Para esto usamos el método Chi-cuadrado de pearson.

```{r}
library(corrplot)
cor(af1, method = 'pearson')
corrplot(cor(af1), tl.srt = 45)
# Mostrando la misma gráfica incluyendo el nivel de correlación numérico
corrplot(cor(af1), tl.srt = 45, 
         method="shade", # cuadrados
         tl.col = "black", addCoef.col = "black")
```

Ahora se puede analizar la relación que existe entre las actividades comerciales y los tipos societario.

```{r}
tab2 = table(datos$actividad, datos$tipo.societario)
cat("\nTipos sociatario:", names(tab2[1,]), "\n")
tab2s = addmargins(tab2)
# la columna 10 es la sumatoria agregada
tab2Ord = tab2s[order(tab2s[,10], decreasing = T),]
af2 = tab2Ord[2:11,1:9]
af3 = tab2Ord[,1:9]
#af2
cat("Tipos societario con más cantidad de actividades:",names(af2[1,1:9]),"\n")
for(tipo in names(af2[1,])) {
  cat(tipo, "\n")
}
plot(af2, las=2, main="Distribución 10 tipos societarios con más actividades comerciales")
```

Ahora se puede graficar la matriz de correlación de tipos societarios y actividades.

```{r}
# Mostrando la misma gráfica incluyendo el nivel de correlación numérico
corrplot(cor(af2, method='pearson'), tl.srt = 45, 
         tl.col = "black")
```
La gráfica de cajas siguiente muestra la dispersión de las actividades comerciales de acuerdo al tipo societario, se puede apreciar una gran dispersión con la mayoría de los valores por debajo de la media.
```{r}
df1 = data.frame(table(datos$actividad, datos$tipo.societario))
# renombrando
names(df1)[names(df1)=="Var1"] = "actividad"
names(df1)[names(df1)=="Var2"] = "tipo_societario"
names(df1)[names(df1)=="Freq"] = "Cantidad"
df1 = df1[df1$tipo_societario != '', ] # filtrando solo tipos societarios no vacíos
colors = rainbow(length(names(table(df1$tipo_societario))))
{
  plot(Cantidad ~ tipo_societario, data=df1, las=2, col=colors)
  legend("topright", legend=names(table(df1$tipo_societario)), cex=0.6, text.col=colors);
}
# Aplicando ANOVA
g = lm(Cantidad ~ tipo_societario, data=df1)  ## realiza la estimación del modelo 
summary(g)
```
